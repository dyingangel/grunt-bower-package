/*
 * grunt-bower-uglify
 * https://bitbucket.org/dyingangel/grunt-bower-package
 *
 * Copyright (c) 2018 Alexander Spies
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerMultiTask('bower_package', 'Build single bower package file...', function () {

        // Merge task-specific and/or target-specific options with these defaults.
        var options = this.options({
                bowerComponentsSrc: 'bower_components',
                targetDir: '',
                targetFilename: 'bowercomponents.js'
            }),
            libOrderArr = [];

        if (!grunt.file.exists("bower.json")) {
            grunt.log.warn('No bower.json file found in your root directory');
            return false;
        }

        /**
         * Used to iterate synchronously and recursively through a folder and fill an array with abs file paths found
         *
         * @param dir
         * @param filelist
         * @returns {*|Array}
         */
        function walkSync(dir, filelist) {
            var path = path || require('path');
            var fs = fs || require('fs'),
                files = fs.readdirSync(dir);
            filelist = filelist || [];
            files.forEach(function (file) {
                if (fs.statSync(path.join(dir, file)).isDirectory()) {
                    filelist = walkSync(path.join(dir, file), filelist);
                }
                else {
                    filelist.push(path.join(dir, file));
                }
            });
            return filelist;
        }

        /**
         * Recursively iterates from a main bower file to all downloaded bower components/libs and return their main JS file
         * we have to inject and looks for other dependencies
         *
         * @param bowerFile
         */
        function getBowerMainFilesRecurse(bowerFile, libOrderArray) {

            if (bowerFile.hasOwnProperty('dependencies')) {

                for (var moduleName in bowerFile.dependencies) {

                    var moduleFiles = [],
                        moduleRootPath = options.bowerComponentsSrc + '/' + moduleName;

                    walkSync(moduleRootPath, moduleFiles);

                    for (var i = 0, l = moduleFiles.length; i < l; i++) {

                        if (moduleFiles[i].indexOf('bower.json') !== -1) {

                            var depBowerFile = grunt.file.readJSON(moduleFiles[i]),
                                libMainFile = depBowerFile.hasOwnProperty('main') ? (moduleRootPath + '/' + depBowerFile.main) : false;

                            //Recursion
                            getBowerMainFilesRecurse(depBowerFile, libOrderArray);

                            if (libMainFile !== false && libOrderArray.indexOf(libMainFile) === -1) {
                                libOrderArray.push(libMainFile);
                                grunt.log.writeln('Bower lib main file found to build a single package => ' + libMainFile);
                            }
                        }
                    }
                }
            }
        }

        //Start recursion from main bower file in project
        getBowerMainFilesRecurse(grunt.file.readJSON("./bower.json"), libOrderArr);

        grunt.log.subhead('Build a single bower file from files listed above...');

        //Set the concat task config
        grunt.config.set('concat', {
            options: {
                separator: ';\n'
            },
            dist: {
                src: libOrderArr,
                dest: options.targetDir + '/' + options.targetFilename
            }
        });

        //Run concatenation
        grunt.task.run('concat');
    });

};
