# grunt-bower-package

> Grunt plugin to build a single JS file from each bower dependencies defined in projects bower.json and their dependencies.

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-bower-package --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-bower-package');
```

## The "bower_package" task

### Overview
In your project's Gruntfile, add a section named `bower_package` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  bower_package: {
        dist: {
            options: {
                bowerComponentsSrc: 'bower_components',
                targetDir: 'build/js/',
                targetFilename: 'libs.js'
            }
        }
    }
});
```

### Options

#### options.bowerComponentsSrc
Type: `String`
Default value: `'bower_components'`

The src folder which contains the downloaded bower components e.g from plugin `grunt-bower-task`

#### options.targetDir
Type: `String`
Default value: `''`

The target dir where the final JS file will be generated

#### options.targetFilename
Type: `String`
Default value: `'bowercomponents.js'`

A string value that is used for the final JS file that is generated

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_
