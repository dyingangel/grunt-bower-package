/*
 * grunt-bower-package
 * https://bitbucket.org/dyingangel/grunt-bower-package
 *
 * Copyright (c) 2018 alexander.spies
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        jshint: {
            all: [
                'Gruntfile.js',
                'tasks/*.js',
                '<%= nodeunit.tests %>'
            ],
            options: {
                jshintrc: '.jshintrc'
            }
        },

        // Before generating any new files, remove any previously-created files.
        clean: {
            tests: ['tmp'],
            libs: ['bower_components'],
            build: ['build']
        },

        bower: {
            dist: {
                options: {
                    install: true,
                    copy: false,
                    verbose: true,
                    layout: 'byComponent'
                }
            }
        },

        // Configuration to be run (and then tested).
        bower_package: {
            dist: {
                options: {
                    bowerComponentsSrc: 'bower_components',
                    targetDir: 'build/js/',
                    targetFilename: 'libs.js'
                }
            }
        },

        // Unit tests.
        nodeunit: {
            tests: ['test/*_test.js']
        }

    });

    // Actually load this plugin's task(s).
    grunt.loadTasks('tasks');

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-nodeunit');
    grunt.loadNpmTasks('grunt-bower-task');

    // Whenever the "test" task is run, first clean the "tmp" dir, then run this
    // plugin's task(s), then test the result.
    grunt.registerTask('test', ['clean', 'bower', 'bower_package', 'nodeunit']);

    // By default, lint and run all tests.
    grunt.registerTask('default', ['jshint', 'test']);
};
